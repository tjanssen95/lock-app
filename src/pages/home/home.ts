import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import * as io from "socket.io-client";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public socket: any;
  public password: string;
  public connectionState = 'disconnected';
  public lockState = 'undetected';  

  constructor(
    public navCtrl: NavController, 
    private nativeStorage: NativeStorage, 
    public alertController: AlertController
  ) 
  {
    // Open een websocket verbinding op het adres '172.24.1.1:3000', 172.24.1.1 is het ipadres, 3000 is de poort waar onze server draait
    this.socket = io("http://172.24.1.1:3000");

    // Als het wachtwoord al een keer is ingevuld, pak deze dan uit de opslag van het apparaat, zo voorkomen we dat we altijd ons wachtwoord
    // moeten typen.
    this.nativeStorage.getItem('password').then(
      // Bestaat het wachtwoord item? Dan gaan we alvast authenticeren!
      data => this.sendPassFast(data),
      // Bij fouten in het ophalen zeggen we dat het wachtwoord opnieuw moet worden ingevoerd, door het wachtwoord een lege string te laten zijn
      error => this.password = ''
    );

    // Wanneer het slot ons zijn 'connectionState' geeft, slaan we deze op in 'this.connectionState'
    this.socket.on('connectionState', function(data){
      this.connectionState = data.connectionState;
    });

    // Hetzelfde voor de lockState (is het slot open of dicht?)
    this.socket.on('lockState', function(data){
      this.lockState = data.lockState;
    });
  }

  // Een functie die als er data wordt gegeven, gelijk authenticeert.
  sendPassFast(data) {
    if (data){
      this.password = data;

      this.authenticate(data);
    }
  }

  // Een functie die wordt uitgevoerd wanneer er nog geen wachtwoord is, het wachtwoord wordt opgeslagen en daarna wordt er geauthenticeert.
  sendPass() {
    this.nativeStorage.setItem('password', this.password)
      .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
      );

    this.authenticate(this.password);
  }

  // Authenticeer, en open daarmee ons slot wanneer het wachtwoord klopt.
  authenticate(password) {
    this.socket.emit('authenticate', {
          password: password
    });
  }

  // Een functie die het slot op slot zet.
  lock() {
    this.socket.emit('lock', {
      lock: true
    });
  }
}
